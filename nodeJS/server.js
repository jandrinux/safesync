var app = require('express')(),
	express = require('express')
  , http = require('http')
  , server = http.createServer(app)
  , io = require('socket.io').listen(server)


var clients = [
	{
		id: 1,
		name: "Enrique 1",
		ip : "200.10.23.14",
		cpu: 0,
		ram: [0,0,0,0,0,0],
		version: "1.3w",
		sync: Date(),
		active: true
	},
    {
    id: 2,
    name: "Enrique Local",
    ip : "200.10.23.14",
    cpu: 0,
    ram: [0,0,0,0,0,0],
    version: "1.3w",
    sync: Date(),
    active: true
  }



  ];


app.use(express.static(__dirname + '/public'));
server.listen(3000)
var sok = null;
io.on('connection',function(socket){
  sok = socket;
  socket.emit('init',{srv: clients})
  socket.on('myClient',function(d){
  	for (var i = 0; i < clients.length; i++) {
  		if (clients[i].id == parseInt(d)) {
  			socket.emit("youClient",(clients[i]));
  			
  		}
  	}
  })
  socket.on('front-terminal',function(data){
    var x = {};
    x.srv = data;
    io.sockets.emit("client-ssh",(x));
  });
  socket.on('bash-result',function(data){
    console.log(data);
    io.sockets.emit("front-ssh",(data));
  });
  socket.on('clients-data',function(data){
  		for (var i = 0; i < clients.length; i++) {

  			 if (clients[i].id == data.id) {
  		
  			 	clients[i].ip = data.ip;
  			 	clients[i].cpu = data.cpu;
  			 	clients[i].ram = data.ram;
  			 	clients[i].version = data.version;
  			 	clients[i].sync=  Date();
  			 	clients[i].active = true;
          io.sockets.emit("details",{srv: clients[i]});
  			 }
  		}
  		io.sockets.emit("respuesta",{srv: clients});
  

  });
socket.on('client-details',function(data){
      for (var i = 0; i < clients.length; i++) {

         if (clients[i].id == data) {
          io.sockets.emit('details', {srv: clients[i]});
          return;
         }
      }
 });
})
app.get('/',function(req,res){
  res.render('/public/index.html')
})