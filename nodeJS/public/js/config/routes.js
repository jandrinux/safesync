angular.module('Safecard').config(['$routeProvider',function($routeProvider){

	$routeProvider.
		when('/', {
			templateUrl: 'js/views/dashboard.html',
			controller: 'DashboardController'
		}).
		when('/details/:Detalleid', {
			templateUrl: 'js/views/details.html',
			controller: 'DashboardController'
		}).
		otherwise({
			redirectTo: '/'
		});
}]);