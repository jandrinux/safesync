angular.module('Safecard').controller('DetailsController', ['$scope','$http','socket','$routeParams','$anchorScroll','$location',
	function($scope,$http,socket,$routeParams,$anchorScroll,$location){
		$scope.params = $routeParams;
		$scope.client = {};
		$scope.terminal = [];
		
		socket.on("connect", function() {
			socket.emit('client-details',$scope.params.Detalleid);		
		});

		socket.on('details',function(data){
				
		if ($scope.params.Detalleid==data.srv.id) {
			$scope.client = data.srv;
				console.log(data.srv);
			var ram = data.srv.ram[0];
			var ramu = data.srv.ram[3];
			var por = (ramu * 100) / ram;
	 		$scope.chartObject.data = [
		 		['Label', 'Value'],
		 		['CPU', parseInt(data.srv.cpu)],
		      	['Memory', parseInt(por)]
		      	
	 		];
 		}

		});
		socket.on('front-ssh',function(data){
			$scope.terminal.push({item:data});
		});
		$scope.color = function(){
			var c = $scope.client;
			if (typeof c.cpu == "undefined") return;
			if(parseInt(c.cpu) > 75 && parseInt(c.cpu) < 90)
				return "gaude-orange";
			if(parseInt(c.cpu) > 90)
				return "gaude-red";

			var ram = c.ram[0];
			var ramu = c.ram[3];
			var por = (ramu * 100) / ram;

			if(parseInt(por) > 75 && parseInt(por) < 90)
				return "gaude-orange";
			if(parseInt(por) > 90)
				return "gaude-red";
			return "gaude-green";
		}

		$scope.entrar = function(e) {
			$scope.terminal.push({item: $scope.txt});
			
			socket.emit("front-terminal",$scope.txt);
			$scope.txt = "";
			$location.hash('bottom');


      		// call $anchorScroll()
      		$anchorScroll();
		}


		$scope.chartObject = {};
	    $scope.chartObject.type = "Gauge";

	    $scope.chartObject.options = {
	      width: 400,
	      height: 157,
	      redFrom: 90,
	      redTo: 100,
	      yellowFrom: 75,
	      yellowTo: 90,
	      minorTicks: 5
	    };

	    $scope.chartObject.data = [
	      ['Label', 'Value'],
	      ['CPU', 0],
	      ['Memory', 0]
	      
	    ];


	}
]);