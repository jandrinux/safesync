angular.module('Safecard').controller('DashboardController', ['$scope','$http','socket','$location',
	function($scope,$http,socket,$location){
		$scope.version = "1.0"
		$scope.clients = [];
		
		socket.on("connect", function() {
			console.log("Conectado al Socket");
		});

		socket.on('init', function (data) {
		   	$scope.clients = data.srv;
		});
		socket.on("respuesta", function (data) {
		   	$scope.clients = data.srv;
		 });
		$scope.ClassSuccess = function(es){
			if (es) 
				return "success"
			else
				return "danger"
		}
		$scope.ClassOK = function(es){
			if (es)
				return "glyphicon glyphicon-ok";
			else
				return "glyphicon glyphicon-remove";
		}
		$scope.irDetalle = function(id){
			$location.path("/details/" + id);
		}
	}
]);