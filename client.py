#!/usr/bin/python

import sys, getopt
import git, os, shutil
from socketIO_client import SocketIO


def on_aaa_response(*args):
    print 'on_aaa_response', args

socketIO = SocketIO('localhost', 3000)
socketIO.on('aaa_response', on_aaa_response)
socketIO.emit('aaa')
socketIO.wait(seconds=1)


def UpServices():
	print "::UpServices::"

def DownServices():
	print "::DownServices::"

def Push():
	REPO = "https://github.com/arodriguezsafe/bk_en.git"
	DIR_NAME = "temp"
	REMOTE_URL = REPO
	if os.path.isdir(DIR_NAME):
	    shutil.rmtree(DIR_NAME)
	 
	os.mkdir(DIR_NAME)
	 
	repo = git.Repo.init(DIR_NAME)
	origin = repo.create_remote('origin',REMOTE_URL)
	origin.fetch()
	origin.pull(origin.refs[0].remote_head)
	print "|----->Push Ok "


print "--SYNC SAFECARD--"

ARG= str(sys.argv)
opts = list(sys.argv)

for o in opts:
	if o=='-h':
		print "-u UpServices"
		print "-d DownServices"
		print "-p Deploy"
	if o=='-u':
		UpServices()
	if o=='-d':
		DownServices()
	if o=='-p':
		Push();
print "-----------------"